# Projeto Fundamentos de Ruby

Este repositório contém exercícios introdutórios para aprender os fundamentos da linguagem Ruby.

## Funcionalidades

- **Exercícios Práticos**: Pequenos desafios para aplicar o que foi aprendido.

## Pré-requisitos

- Ruby 2.5 ou superior instalado.

## Como Usar

1. Clone este repositório:
   ```
   git clone https://gitlab.com/learning-skill/ruby/fundamentos.git
   ```

2. Navegue até o diretório do projeto:
   ```
   cd fundamentos
   ```

3. Execute os scripts conforme necessário, um exemplo: 
   ```
   ruby check_tests.rb
   ```