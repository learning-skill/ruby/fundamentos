# Desafio
# Você acabou de aprender POO e foi desafiado
# a criar o seu primeiro projeto em Ruby on Rails. 
# Neste código, você deve criar uma classe chamada 
# Pessoa com  dois atributos: nome e idade. Em seguida, 
# crie um objeto do tipo Pessoa e seus atributos.

# Requisitos:

# Utilizar classes e métodos para implementar o programa;
# O programa deve solicitar que o usuário insira o nome e a idade da pessoa;
# O programa deve exibir o nome e a idade da pessoa inserida pelo usuário.

class Pessoa
  attr_accessor :nome, :idade
#TODO: Implemente a classe Pessoa
 def initialize(nome, idade)
   @nome = self.nome= nome
   @idadae= self.idade= idade
 end
end

class Program
  def self.main
    puts "Digite o nome da pessoa"
    nome = gets.chomp

    puts "Digite a idade da pessoa"
    idade = gets.chomp.to_i

    pessoa = Pessoa.new(nome, idade)

    puts "Nome: #{pessoa.nome}, Idade: #{pessoa.idade}"
  end
end

Program.main