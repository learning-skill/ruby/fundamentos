class Robo
  def initialize(nome, modelo, ano_fabricacao)
    @nome = nome
    @modelo = modelo
    @ano_fabricacao = ano_fabricacao    
  end

  def exibir_informacoes
    puts "O robô #{@nome}, modelo #{@modelo}, foi fabricado em #{@ano_fabricacao}"
  end
end

puts "Digite o numero de robô do seu exercito:"
exercito_total = gets.chomp.to_i
exercito = []
x = 1
while x <= exercito_total
  nome = "robo_#{x}"
  puts "digite o modelo do #{x}º robô"
    modelo = gets.chomp
  puts "digite o ano de fabricação do #{x+1}º robô"
    ano = gets.chomp.to_i
  x += 1

  meu_robo = Robo.new(nome, modelo, ano)
  exercito << meu_robo
end

exercito.each do |robo|
  robo.exibir_informacoes
end
