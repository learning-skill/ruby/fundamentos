# Entrada de dados
data_inicial_str = gets.chomp
data_final_str = gets.chomp

class SistemaAcionistas
  def obterAnalisesDesempenho(data_inicial_str, data_final_str)
    data_inicial = parse_data(data_inicial_str)
    data_final = parse_data(data_final_str)

		# Lista de análises com datas e descrições.
    analises = [
      Analise.new(parse_data("01/01/2023"), "Analise de Desempenho Financeiro"),
      Analise.new(parse_data("15/02/2023"), "Analise de Riscos e Exposicoes"),
      Analise.new(parse_data("31/03/2023"), "Analises Corporativas"),
      Analise.new(parse_data("01/04/2023"), "Analise de Politicas e Regulamentacoes"),
      Analise.new(parse_data("15/05/2023"), "Analise de Ativos"),
      Analise.new(parse_data("30/06/2023"), "Analise de Inovacao e Tecnologia")
    ]

		# TODO: Filtre as análises que estão dentro do intervalo de datas especificado.
    data_inicial = parse_data(data_inicial_str)
    data_final = parse_data(data_final_str)

    analises_filtradas = analises.select do |analise|
      analise.data >= data_inicial && analise.data <= data_final
    end

    descricoes_analises = analises_filtradas.map(&:descricao)
    return descricoes_analises
  
    # TODO: Retorne apenas as descrições das análises filtradas.
  end

  private

	# Método privado para analisar a string de data e converter para um objeto de data.
  def parse_data(data_str)
    dia, mes, ano = data_str.split('/').map(&:to_i)
    Time.new(ano, mes, dia)
  end
end

# Classe Analise que representa uma análise com uma data e uma descrição.
class Analise
  attr_reader :data, :descricao

  # TODO: Inicialize uma análise com uma data e uma descrição.
  def initialize(data, descricao)
    @data = data
    @descricao = descricao
  end
end

sistema_acionistas = SistemaAcionistas.new

analises_desempenho = sistema_acionistas.obterAnalisesDesempenho(data_inicial_str, data_final_str)
# TODO: Faça a chamada do método para obter análises de desempenho dentro do intervalo especificado.
puts "Análises de desempenho no período de #{data_inicial_str} a #{data_final_str}:"
analises_desempenho.each do |descricao|
  puts "- #{descricao}"
end