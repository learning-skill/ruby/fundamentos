# Desafio
# Crie um programa que, dadas duas 
# listas de números inteiros e tamanho igual, 
# some os índices pares de cada uma delas e 
# print no console uma nova lista resultante 
# dessa operação.
puts "digite os valores do primeiro array"
input_array1 = gets.chomp.split.map(&:to_i)
puts "digite os valores do segundo array"
input_array2 = gets.chomp.split.map(&:to_i)
def sum_array(input_array1, input_array2)
  output_array = []
  x = 0 
  while x <= input_array1.length-1 do
    output_array << input_array1[x] + input_array2[x] if x % 2 == 0
    x += 1
  end 
  
  puts output_array.join(',')
end

sum_array(input_array1, input_array2)

