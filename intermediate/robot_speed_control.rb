# Desafio
# Você foi contratado para criar um software que simule o controle 
# de velocidade de um robô. Esse robô possui uma velocidade máxima e 
# uma velocidade mínima. Sua tarefa é desenvolver um programa 
# utilizando o conceito de Orientação a Objetos para calcular a velocidade 
# final do robô após uma sequência de comandos. Crie uma classe chamada "Robo"
# que possua as seguintes propriedades e métodos:

# velocidadeAtual: inteiro que representa a velocidade atual do robô (inicialmente 0);
# velocidadeMaxima: inteiro que representa a velocidade máxima do robô;
# velocidadeMinima: inteiro que representa a velocidade mínima do robô;
# acelerar(): um método que aumenta a velocidade atual em 1 unidade, desde que não ultrapasse a velocidade máxima;
# desacelerar(): um método que diminui a velocidade atual em 1 unidade, desde que não fique abaixo da velocidade mínima.

class Robo
  attr_accessor :velocidade_atual
  attr_reader :velocidade_maxima, :velocidade_minima

  def initialize(vmin, vmax)
    @velocidade_minima = vmin
    @velocidade_maxima = vmax
    @velocidade_atual = vmin
  end

  def acelerar
    #TODO: Implementar a lógica especificada para o método "acelerar".
    @velocidade_atual += 1
  end

  def desacelerar
   #TODO: Implementar a lógica especificada para o método "acelerar".
   @velocidade_atual -= 1
  end
end
puts "Digite o velocidade maxima e a minima"
vmin, vmax = gets.chomp.split.map(&:to_i)

puts "Digite os comandos"
comandos = gets.chomp

robo = Robo.new(vmin, vmax)

comandos.each_char do |comando|
  #TODO: Considerar os comandos para invocar seu respectivo método.
  if comando.casecmp("a") == 0
    robo.acelerar
    puts "você está acelerando, #{robo.velocidade_atual}"
  elsif comando.casecmp("d")
    puts "você está freiando, #{robo.desacelerar}"
  else
    puts "entre com um comando valido! Ex.: 'A' e 'D'"
  end
end

