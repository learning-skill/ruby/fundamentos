class Analise
  attr_reader :data, :descricao

  def initialize(data, descricao)
    @data = data
    @descricao = descricao
  end
end

class SistemaAcionistas
  def initialize
    @analises = [
      Analise.new(parse_data("01/01/2023"), "Análise de Desempenho Financeiro"),
      Analise.new(parse_data("15/02/2023"), "Análise de Riscos e Exposições"),
      Analise.new(parse_data("31/03/2023"), "Análises Corporativas"),
      Analise.new(parse_data("01/04/2023"), "Análise de Políticas e Regulamentações"),
      Analise.new(parse_data("15/05/2023"), "Análise de Ativos"),
      Analise.new(parse_data("30/06/2023"), "Análise de Inovação e Tecnologia")
    ]
  end

  def obterAnalisesDesempenho(data_inicial_str, data_final_str)
    data_inicial = parse_data(data_inicial_str)
    data_final = parse_data(data_final_str)

    analises_filtradas = @analises.select do |analise|
      analise.data >= data_inicial && analise.data <= data_final
    end

    descricoes_analises = analises_filtradas.map(&:descricao)
    return descricoes_analises
  end

  private

  def parse_data(data_str)
    dia, mes, ano = data_str.split('/').map(&:to_i)
    Time.new(ano, mes, dia)
  end
end

sistema_acionistas = SistemaAcionistas.new

print "Digite a data inicial (dd/mm/aaaa): "
data_inicial_str = gets.chomp

print "Digite a data final (dd/mm/aaaa): "
data_final_str = gets.chomp

analises_desempenho = sistema_acionistas.obterAnalisesDesempenho(data_inicial_str, data_final_str)

puts "Análises de desempenho no período de #{data_inicial_str} a #{data_final_str}:"
analises_desempenho.each do |descricao|
  puts "- #{descricao}"
end