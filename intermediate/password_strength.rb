def verificar_forca_senha(senha)
  comprimento_minimo = 8
  tem_letra_minuscula = /[a-z]/.match?(senha)
  tem_letra_minuscula = /[A-Z]/.match?(senha)
  tem_numero = /\d/.match?(senha)
  tem_char_especial = /\W/.match?(senha)

  senha_minuscula = senha.downcase

  tem_senquencia = senha_minuscula.include?("123456") || senha_minuscula.include?("abcdef")

  tem_palavra_comun = senha_minuscula == "password" || senha_minuscula == "123456" || senha_minuscula == "qwerty"

  if ((senha.length >= comprimento_minimo)) && (tem_letra_minuscula) && tem_letra_minuscula && tem_numero && tem_char_especial && !tem_senquencia && !tem_palavra_comun
    puts "Sua senha atende aos requisitos de segurança. Parabéns!"
  elsif senha.length < comprimento_minimo
    puts "Sua senha é muito curta. Recomenda-se o minimo de 8 caracteres"
  else 
    puts "Sua senha não atende aos requisitos de segurança."
  end

end

puts "Digite a senha que deseja verificar a força: "
senha = gets.chomp
resultado = verificar_forca_senha(senha)
puts resultado