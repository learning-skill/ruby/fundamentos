# Desafio
# Você foi contratado para criar um programa que 
# mostrasse para os clientes o preço do gás de 
# cozinha. Para isso foi explicado que o preço 
# variava todos os meses por conta de impostos 
# que eram alterados pelas refinarias em conjunto 
# com o governo. Então, existe um imposto todo mês
# de 10% e um variável de acordo com a bolsa e 
# vendas que poderia, ou não, estar presente mês a mês
# e que é cobrado com base no preço após o calculo do 
# imposto de todo mês.
# Crie um programa que faça essa verificação e print no 
# console a seguinte frase: “O preço do gás nesse mês é de R$...”.
# Lembrando que 0 representa false e 1 representa true.

puts "Digite o preço do gás"
precoDoGas = gets.to_f
imposto = 0.1
impostoCobrado = gets.to_i
impostoVariavel = gets.to_f
def final_price(precoDoGas, impostoVariavel, impostoCobrado, imposto)
  
  flat_rate = precoDoGas * imposto
  final_price = precoDoGas + flat_rate

  final_price += final_price * (impostoVariavel/100.0) if impostoCobrado == 1


  if final_price == final_price.floor
    puts "O preço do gás nesse mês é de R$#{final_price.to_i}"
  else
    puts "O preço do gás nesse mês é de R$#{final_price.round(2)}"
  end
end

final_price(precoDoGas, impostoVariavel, impostoCobrado, imposto)

