class Personagem
  def initialize(nome, mana)
    @nome = nome
    @mana = mana    
  end
end

class Subclasse < Personagem
  attr_accessor :dano_base

  def initialize(nome, mana, dano_base)
    super(nome, mana)
    @dano_base = dano_base
  end

  def calcular_dano
    puts "#{@nome} atacou e casou #{@dano_base * @mana} de dano!"
  end
end

puts "Digite o nome do personagem:"
nome = gets.chomp

puts "Digite a mana do personagem:"
mana = gets.chomp.to_i

puts "Digite o dano base do personagem:"
dano_base = gets.chomp.to_i

personagem = Subclasse.new(nome, mana, dano_base)
personagem.calcular_dano
