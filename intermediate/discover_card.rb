class Carta
  attr_reader :naipe, :valor
  def initialize(naipe, valor)
    @naipe = naipe
    @valor = valor
  end
end

module Naipe
  Paus = 0
  Ouros = 1
  Copas = 2
  Espadas = 3
end

module Valor
  As = 1
  Valete = 2
  Dama = 3
  Rei = 4
end

valor_escolhido = loop do 
  puts "Escolha de acordo com a associação a seguir:
  As = 1, Valete = 2, Dama = 3, Rei = 4"
  valor = gets.chomp.to_i
  (1..4).cover?(valor) ? break : "escolha fora da associação" if valor
end

naipe_escolhido = loop do
  puts "Escolha de acordo com a associação a seguir:
  Paus = 0, Ouros = 1, Copas = 2, Espadas = 3"
  naipe = gets.chomp.to_i
  break naipe if (0..3).cover?(naipe)
end

carta_escolhida = Carta.new(naipe_escolhido, valor_escolhido)

nome_valor = ""
nome_naipe = ""
case valor_escolhido
when 1
  nome_valor = "Ás"
when 2
  nome_valor = "Valete"
when 3
  nome_valor = "Dama"
when 4
  nome_valor = "Rei"
end

case naipe_escolhido
when 0
  nome_naipe = "Paus"
when 1
  nome_naipe = "Ouros"
when 2
  nome_naipe = "Copas"
when 3
  nome_naipe = "Espadas"
end

puts "Carta escolhida: #{nome_valor} de #{nome_naipe}"

